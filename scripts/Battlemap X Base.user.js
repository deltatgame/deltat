// ==UserScript==
// @name         Battlemap X Base
// @namespace    http://tampermonkey.net/
// @version      0.6
// @description  try to take over the world!
// @author       LaFleur
// @updateURL    https://gitlab.com/deltatgame/deltat/raw/master/scripts/Battlemap%20X%20Base.user.js
// @downloadURL  https://gitlab.com/deltatgame/deltat/raw/master/scripts/Battlemap%20X%20Base.user.js
// @match        https://battlemap.deltatgame.com/home
// @grant        none
// ==/UserScript==


/// Generic contain function cuz javascript fails on this.
if (!String.prototype.contains) {
    String.prototype.contains = function(s) {
        return this.indexOf(s) > -1
    }
}

$(function(){
    var map = mapController.returnMap();

    var MC_array = getCurrentMC();
    console.log(MC_array);

    map.on('dragend', function() {
       'use strict';
       console.log("You dragged the Map !");

        // Main faction
        baseListController.showFactionBases({ "faction": MC_array[0], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
        var zoom = map.getZoom();
        if (zoom >= 9)
        {
        // Oppo factions
        baseListController.showFactionBases({ "faction": MC_array[1], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
        baseListController.showFactionBases({ "faction": MC_array[2], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
        baseListController.showFactionBases({ "faction": MC_array[3], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');


        }

    });

    map.on('zoomend',function() {
    'use strict';
    console.log("You Zoomed in or out Map !");
        // cosmo
        baseListController.showFactionBases({ "faction": MC_array[0], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
        var zoom = map.getZoom();
        if (zoom >= 9)
        {
        // genex
        baseListController.showFactionBases({ "faction": MC_array[1], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
       // humanoid
        baseListController.showFactionBases({ "faction": MC_array[2], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
        // nyoko
        baseListController.showFactionBases({ "faction": MC_array[3], 'minLevel': 1, 'bounds': mapController.getMapBounds()}, 'true');
        }


});
});

function getCurrentMC()
{
    /// Generic fucntion to return faction ID's where the first faction ID is the players faction ID and the other 3 are the opposing faction ID's.

    var mc_button = document.getElementById("player-faction-icon");
    var mc_choice = mc_button.outerHTML;
    var MC_array = [];

    if (mc_choice.contains('cosmostellar'))
    {
        console.log('cosmooo');

        MC_array.push('2');

        MC_array.push('1');
        MC_array.push('3');
        MC_array.push('4');

    }

    if (mc_choice.contains('humanoid'))
    {
        console.log('borg');

        MC_array.push('4');

        MC_array.push('1');
        MC_array.push('2');
        MC_array.push('3');
    }

    if (mc_choice.contains('nyoko-labs'))
    {
        console.log('seal');

        MC_array.push('1');

        MC_array.push('2');
        MC_array.push('3');
        MC_array.push('4');
    }

        if (mc_choice.contains('gene-x'))
    {
        console.log('mutant');

        MC_array.push('3');

        MC_array.push('1');
        MC_array.push('2');
        MC_array.push('4');
    }

    return MC_array;
}



// // Snip from battlemap script // //
//
//


var baseListController = (function() {
	var baseTimeout;
	var baseFactionTimeout;
	var baseTotalCount,
		baseLoadedCountUpdated,
		baseLoadedCount = 0;

	var factionTotalCount,
		factionLoadedCountUpdated,
		factionLoadedCount = 0;

	return {
		clearTimeout: function() {
			clearTimeout(baseTimeout);
			progressParams.config.baseCount = 0;
			progressParams.configTotal.baseTotalCount = 0;
			progressParams.getProgressBarPercentage();
		},
		clearFactionTimeout: function () {
			clearTimeout(baseFactionTimeout);
			progressParams.config.factionFilterCount = 0;
			progressParams.configTotal.factionFilterTotalCount = 0;
			progressParams.getProgressBarPercentage();
		},
		showBases: function(data, factionFilter = 'false') {
			// console.log('show bases!');
			var bases = baseController.getBases(data);
			mapController.setMarkers(bases['bases'], 'base', factionFilter, bases['playerFactionEnum']);
			localStorage.setItem("playerFactionEnum", bases['playerFactionEnum']);

	        if(bases['lastID'] !== 0) {
				progressParams.updateTotalCount('base', bases['count']);
				progressParams.updateConfigCount('base', Object.keys(bases['bases']).length);

				progressParams.getProgressBarPercentage();

				baseTimeout = setTimeout(function() {
					data.bases = bases;
					baseListController.showBases(data);
				}, 10);
			}
		},
		showFactionBases: function(data, factionFilter = 'true') {
			var bases = baseController.getBases(data);

			mapController.setMarkers(bases['bases'], 'base', factionFilter, bases['playerFactionEnum']);

	        if(bases['lastID'] !== 0) {
				progressParams.updateTotalCount('factionFilter', bases['count']);
				progressParams.updateConfigCount('factionFilter', Object.keys(bases['bases']).length);

				progressParams.getProgressBarPercentage();

				baseFactionTimeout = setTimeout(function () {
					data.bases = bases;
					baseListController.showFactionBases(data, factionFilter = 'true');
				}, 10);
			}
	    }
	};
})();