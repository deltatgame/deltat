## Chrome

Although it is possible to install userscripts directly as extensions, the recommended method is to use Tampermonkey. Once Tampermonkey is installed, click on the "Download" behind the links below and follow the instructions.
https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

## Firefox

Install the Greasemonkey Firefox add-on. Once installed, click the "Download" behind the links below then "Install" on the dialog.
https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/

## Other browsers

Check your browser documentation for details on installing userscripts.


# scripts

* [Battlemap_X_POI](https://gitlab.com/deltatgame/deltat/raw/master/scripts/Battlemap%20X%20POI.user.js)
  * Shows all cores/mines on map from a hgher zoomlevel (credits: @LaFleur_deltat)
* [Battlemap_X_Base](https://gitlab.com/deltatgame/deltat/raw/master/scripts/Battlemap%20X%20Base.user.js)
  * Shows all bases on map from a higher zoomlevel (credits: @LaFleur_deltat)

