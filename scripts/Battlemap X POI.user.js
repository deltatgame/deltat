// ==UserScript==
// @name         Battlemap X POI
// @namespace    http://tampermonkey.net/
// @updateURL      https://gitlab.com/deltatgame/deltat/raw/master/scripts/Battlemap%20X%20POI.user.js
// @downloadURL    https://gitlab.com/deltatgame/deltat/raw/master/scripts/Battlemap%20X%20POI.user.js
// @version      0.2
// @description  try to take over the world!
// @author       LaFleur
// @match        https://battlemap.deltatgame.com/home
// @grant        none
// ==/UserScript==


$(function(){
    var map = mapController.returnMap();
    console.log(map);

    map.on('dragend', function() {
       'use strict';
       console.log("You dragged the Map !");
        var zoom = map.getZoom();
        if (zoom >= 12)
        {
            coreListController.showCores({ 'bounds': mapController.getMapBounds() });
            POIListController.showPOIs({ 'bounds': mapController.getMapBounds() });
        }
    });

    map.on('zoomend',function() {
    'use strict';
    console.log("You Zoomed in or out Map !");

    var zoom = map.getZoom();
    if (zoom >= 12)
    {
        coreListController.showCores({ 'bounds': mapController.getMapBounds() });
        POIListController.showPOIs({ 'bounds': mapController.getMapBounds() });
    }

});
});



// // Snip from battlemap script // //
//
//

var coreController = (function(ajaxController) {
	return {
		getCores: function(data) {
			data.minLevel = data.minLevel !== undefined ? data.minLevel : 1;
			data.maxLevel = data.maxLevel !== undefined ? data.maxLevel : 5;
			data.minHealth = data.minHealth !== undefined ? data.minHealth : 0;
			data.maxHealth = data.maxHealth !== undefined ? data.maxHealth : 100;
			data.coreLastID = data.cores !== undefined ? data.cores['lastID'] : 0;
			data.cores = [];

			var cores = ajaxController.getValues('get-cores', 'post', data);
			return cores;
		},
		getNeutralCores: function(data) {
			data.minLevel = data.minLevel !== undefined ? data.minLevel : 1;
			data.maxLevel = data.maxLevel !== undefined ? data.maxLevel : 5;
			data.minHealth = data.minHealth !== undefined ? data.minHealth : 0;
			data.maxHealth = data.maxHealth !== undefined ? data.maxHealth : 100;
			data.coreLastID = data.cores !== undefined ? data.cores['lastID'] : 0;
			data.cores = [];

			var cores = ajaxController.getValues('get-neutral-cores', 'post', data);
			return cores;
		}
	};
})(ajaxController);

var coreListController = (function(coreController) {
	var coreTimeout;
	var neutralCoreTimeout;

	return {
		clearCoreTimeout: function() {
			clearTimeout(coreTimeout);
			progressParams.config.coresCount = 0;
			progressParams.configTotal.coresTotalCount = 0;
			progressParams.getProgressBarPercentage();
		},
		clearNeutralCoreTimeout: function() {
			clearTimeout(neutralCoreTimeout);
			progressParams.config.neutralCoresCount = 0;
			progressParams.configTotal.neutralCoresTotalCount = 0;
			progressParams.getProgressBarPercentage();
		},
		showCores: function(data) {
			var cores = coreController.getCores(data);
	        mapController.setMarkers(cores['cores'], 'core');
	        if(cores['lastID'] !== 0) {
				progressParams.updateTotalCount('core', cores['count']);
				progressParams.updateConfigCount('core', Object.keys(cores['cores']).length);

				progressParams.getProgressBarPercentage();

				coreTimeout = setTimeout(function() {
					data.cores = cores;
	                coreListController.showCores(data);
	            }, 2000);
			}
		},
		showNeutralCores: function(data) {
			var neutralCores = coreController.getNeutralCores(data);
	        mapController.setMarkers(neutralCores['cores'], 'core');
	        if(neutralCores['lastID'] !== 0) {
				progressParams.updateTotalCount('neutralCore', neutralCores['count']);
				progressParams.updateConfigCount('neutralCore', Object.keys(neutralCores['cores']).length);

				progressParams.getProgressBarPercentage();

				neutralCoreTimeout = setTimeout(function() {
					data.cores = neutralCores;
	                coreListController.showNeutralCores(data);
	            }, 2000);
			}
		}
	};
})(coreController);

    var POIListController = (function(POIController) {
        var POITimeout;
        var neutralPOITimeout;

        return {
            clearPOITimeout: function() {
                clearTimeout(POITimeout);
                progressParams.config.POIsCount = 0;
                progressParams.configTotal.POIsTotalCount = 0;
                progressParams.getProgressBarPercentage();
            },
            clearNeutralPOITimeout: function() {
                clearTimeout(neutralPOITimeout);
                progressParams.config.neutralPOIsCount = 0;
                progressParams.configTotal.neutralPOIsTotalCount = 0;
                progressParams.getProgressBarPercentage();
            },
            showPOIs: function(data) {
                var POIs = POIController.getPOIs(data);
                mapController.setMarkers(POIs['POIs'], 'POI');
                if(POIs['lastID'] !== 0) {
                    progressParams.updateTotalCount('POI', POIs['count']);
                    progressParams.updateConfigCount('POI', Object.keys(POIs['POIs']).length);

                    progressParams.getProgressBarPercentage();

                    POITimeout = setTimeout(function() {
                        data.POIs = POIs;
                        POIListController.showPOIs(data);
                    }, 2000);
                }
            },
            showNeutralPOIs: function(data) {
                var neutralPOIs = POIController.getNeutralPOIs(data);
                mapController.setMarkers(neutralPOIs['POIs'], 'POI');
                if(neutralPOIs['lastID'] !== 0) {
                    progressParams.updateTotalCount('neutralPOI', neutralPOIs['count']);
                    progressParams.updateConfigCount('neutralPOI', Object.keys(neutralPOIs['POIs']).length);

                    progressParams.getProgressBarPercentage();

                    neutralPOITimeout = setTimeout(function() {
                        data.POIs = neutralPOIs;
                        POIListController.showNeutralPOIs(data);
                    }, 2000);
                }
            }
        };
    })(POIController);


var baseListController = (function() {
	var baseTimeout;
	var baseFactionTimeout;
	var baseTotalCount,
		baseLoadedCountUpdated,
		baseLoadedCount = 0;

	var factionTotalCount,
		factionLoadedCountUpdated,
		factionLoadedCount = 0;

	return {
		clearTimeout: function() {
			clearTimeout(baseTimeout);
			progressParams.config.baseCount = 0;
			progressParams.configTotal.baseTotalCount = 0;
			progressParams.getProgressBarPercentage();
		},
		clearFactionTimeout: function () {
			clearTimeout(baseFactionTimeout);
			progressParams.config.factionFilterCount = 0;
			progressParams.configTotal.factionFilterTotalCount = 0;
			progressParams.getProgressBarPercentage();
		},
		showBases: function(data, factionFilter = 'false') {
			// console.log('show bases!');
			var bases = baseController.getBases(data);
			mapController.setMarkers(bases['bases'], 'base', factionFilter, bases['playerFactionEnum']);
			localStorage.setItem("playerFactionEnum", bases['playerFactionEnum']);

	        if(bases['lastID'] !== 0) {
				progressParams.updateTotalCount('base', bases['count']);
				progressParams.updateConfigCount('base', Object.keys(bases['bases']).length);

				progressParams.getProgressBarPercentage();

				baseTimeout = setTimeout(function() {
					data.bases = bases;
					baseListController.showBases(data);
				}, 2000);
			}
		},
		showFactionBases: function(data, factionFilter = 'true') {
			var bases = baseController.getBases(data);
			mapController.setMarkers(bases['bases'], 'base', factionFilter, bases['playerFactionEnum']);

	        if(bases['lastID'] !== 0) {
				progressParams.updateTotalCount('factionFilter', bases['count']);
				progressParams.updateConfigCount('factionFilter', Object.keys(bases['bases']).length);

				progressParams.getProgressBarPercentage();

				baseFactionTimeout = setTimeout(function () {
					data.bases = bases;
					baseListController.showFactionBases(data, factionFilter = 'true');
				}, 2000);
			}
	    }
	};
})();